package com.rijatru.development.useraccountmanager;

import com.rijatru.development.useraccountmanager.businessLogic.encryption.EncryptionUtil;
import com.rijatru.development.useraccountmanager.businessLogic.encryption.Encryptor;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

public class EncryptorTest {
    @Test
    public void encryptData_nullValues_logsError() {
        Encryptor encryptor = new EncryptionUtil();

        HashMap<String, byte[]> map = encryptor.encryptData(null, null);
        Assert.assertNull(map);
    }

    @Test
    public void encryptData_emptyString_logsError() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);
        Assert.assertNull(map);
    }

    @Test
    public void encryptData_validString_validMap() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);
        Assert.assertNotNull(map);
    }

    @Test
    public void encryptData_validString_saltIsNotNull() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);
        Assert.assertNotNull(map.get("salt"));
    }

    @Test
    public void encryptData_validString_initializationVectorIsNotNull() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);
        Assert.assertNotNull(map.get("initializationVector"));
    }

    @Test
    public void encryptData_validString_encryptedBytesIsNotNull() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);
        Assert.assertNotNull(map.get("encryptedBytes"));
    }

    @Test
    public void encryptData_validString_differentSaltForTheSamePasswordTwice() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map1 = encryptor.encryptData(bytes, password);
        HashMap<String, byte[]> map2 = encryptor.encryptData(bytes, password);

        byte salt1[] = map1.get("salt");
        byte salt2[] = map2.get("salt");

        Assert.assertFalse(Arrays.equals(salt1, salt2));
    }

    @Test
    public void encryptData_validString_differentEncryptedBytesForTheSamePasswordTwice() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map1 = encryptor.encryptData(bytes, password);
        HashMap<String, byte[]> map2 = encryptor.encryptData(bytes, password);

        byte encryptedBytes1[] = map1.get("encryptedBytes");
        byte encryptedBytes2[] = map2.get("encryptedBytes");

        Assert.assertFalse(Arrays.equals(encryptedBytes1, encryptedBytes2));
    }

    @Test
    public void decryptData_validString_sameEncryptedAndDecryptedPassword() {
        Encryptor encryptor = new EncryptionUtil();

        String password = "SecurePassword";
        byte[] bytes = password.getBytes();

        HashMap<String, byte[]> map = encryptor.encryptData(bytes, password);

        byte[] decrypted = encryptor.decryptData(map, "SecurePassword");
        String decryptedPassword = new String(decrypted);

        Assert.assertEquals(password, decryptedPassword);
    }
}
