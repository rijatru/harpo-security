package com.rijatru.development.useraccountmanager;

import android.content.Context;

import com.rijatru.development.useraccountmanager.businessLogic.encryption.EncryptionUtil;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManagerImplementation;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class UserCredentialsManagerTest {
    @Test
    public void saveUserCredentials_nullObject_logsError() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());
        Context context = Mockito.mock(Context.class);

        boolean credentialsHasBeenSaved = userCredentialsManager.saveUserCredentials(context,null);
        Assert.assertFalse(credentialsHasBeenSaved);
    }
}
