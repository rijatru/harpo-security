package com.rijatru.development.useraccountmanager;

import com.rijatru.development.useraccountmanager.data.managers.AppApiManager;
import com.rijatru.development.useraccountmanager.data.managers.AppPersistenceManager;
import com.rijatru.development.useraccountmanager.data.managers.RetroFitManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;
import com.rijatru.development.useraccountmanager.data.services.ApiConfig;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedApiConfigImplementation;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedApp;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedAppConnectivityManager;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedCustomLocationManager;
import com.rijatru.development.useraccountmanager.rules.TestSchedulerRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;
import io.reactivex.observers.TestObserver;

import static java.util.concurrent.TimeUnit.SECONDS;

@RunWith(AndroidJUnit4.class)
public class PersistenceManagerInstrumentedTest {

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    private PersistenceManager getPersistenceManager() {
        UserAccountManagerApp app = new MockedApp();
        ApiConfig apiConfig = new MockedApiConfigImplementation();
        HttpManager httpManager = new RetroFitManager(apiConfig);
        ConnectivityStatusManager connectivityStatusManager = new MockedAppConnectivityManager();
        ApiManager apiManager = new AppApiManager(httpManager, apiConfig, connectivityStatusManager);
        CustomLocationManager customLocationManager = new MockedCustomLocationManager(app);
        return new AppPersistenceManager(app, apiManager, customLocationManager);
    }

    @Test
    public void saveValidationTry_validObject_validResponse() {

        PersistenceManager persistenceManager = getPersistenceManager();

        AccountValidationTry accountValidationTry = new AccountValidationTry();
        accountValidationTry.setTryDate("date-info");
        accountValidationTry.setSuccess(true);

        TestObserver<AccountValidationTry> testObserver = persistenceManager.saveAccountValidationTry(accountValidationTry).test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(2, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(response -> {
                    return response.getId() > 0;
                });
    }

    @Test
    public void saveValidationTry_invalidObject_logsError() {

        PersistenceManager persistenceManager = getPersistenceManager();

        AccountValidationTry accountValidationTry = new AccountValidationTry();

        TestObserver<AccountValidationTry> testObserver = persistenceManager.saveAccountValidationTry(accountValidationTry).test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(1, SECONDS);

        testObserver
                .assertError(error -> {
                    return error.getMessage().equals("Invalid validation entry object");
                });
    }

    @Test
    public void getAllValidationTries_all_validObjectsList() {

        PersistenceManager persistenceManager = getPersistenceManager();

        TestObserver<List<AccountValidationTry>> testObserver = persistenceManager.getAllAccountValidationTries().test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(1, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(response -> {
                    return response.size() >= 0;
                });
    }

    @Test
    public void deleteAllValidationTries_all_returnsTrue() {

        PersistenceManager persistenceManager = getPersistenceManager();

        TestObserver<Integer> testObserver = persistenceManager.deleteAllAccountValidationTries().test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(1, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(deletedRows -> {
                    return deletedRows >= 0;
                });
    }

    @Test
    @LargeTest
    public void saveCredentials_validObject_returnsTrue() {

        PersistenceManager persistenceManager = getPersistenceManager();

        TestObserver<Boolean> testObserver = persistenceManager.saveValidationTry(true).test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(3, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(success -> {
                    return success == true;
                });
    }
}
