package com.rijatru.development.useraccountmanager.mockedDependencies;

import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;

public class MockedAppConnectivityManager implements ConnectivityStatusManager {

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void setIsConnected(boolean isConnected) {

    }
}
