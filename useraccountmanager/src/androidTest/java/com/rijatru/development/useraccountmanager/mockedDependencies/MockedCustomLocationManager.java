package com.rijatru.development.useraccountmanager.mockedDependencies;

import android.location.Location;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.data.managers.LocationManagerImplementation;

public class MockedCustomLocationManager extends LocationManagerImplementation {

    public MockedCustomLocationManager(UserAccountManagerApp app) {
        super(app);
    }

    @Override
    public void getLocation() {
        Location location = new Location("");
        location.setLatitude(4.5555);
        location.setLongitude(-74.5555);
        locationSubject.onNext(location);
    }
}
