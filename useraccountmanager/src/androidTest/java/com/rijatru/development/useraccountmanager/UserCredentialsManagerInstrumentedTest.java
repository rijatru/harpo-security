package com.rijatru.development.useraccountmanager;

import android.content.Context;

import com.rijatru.development.useraccountmanager.businessLogic.encryption.EncryptionUtil;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManagerImplementation;
import com.rijatru.development.useraccountmanager.data.model.userCredentials.UserCredentials;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

@RunWith(AndroidJUnit4.class)
public class UserCredentialsManagerInstrumentedTest {

    private Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void saveUserCredentials_validObject_returnsTrue() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());

        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setUsername("user@mail.com");
        userCredentials.setPassword("securePassword.55@");

        boolean credentialsHasBeenSaved = userCredentialsManager.saveUserCredentials(context, userCredentials);
        Assert.assertTrue(credentialsHasBeenSaved);
    }

    @Test
    public void getUserCredentials_validPassword_validCredentials() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());

        UserCredentials userCredentials = userCredentialsManager.getUserCredentials(context, "securePassword.55@");
        Assert.assertNotNull(userCredentials);
    }

    @Test
    public void getUserCredentials_invalidPassword_nullObject() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());

        UserCredentials userCredentials = userCredentialsManager.getUserCredentials(context, "invalidPassword.55@");
        Assert.assertNull(userCredentials);
    }

    @Test
    public void getUserCredentials_nullPassword_logsErrorReturnsNullObject() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());

        UserCredentials userCredentials = userCredentialsManager.getUserCredentials(context, null);
        Assert.assertNull(userCredentials);
    }

    @Test
    public void getUserCredentials_emptyPassword_logsErrorReturnsNullObject() {
        UserCredentialsManager userCredentialsManager = new UserCredentialsManagerImplementation(new EncryptionUtil());

        UserCredentials userCredentials = userCredentialsManager.getUserCredentials(context, "");
        Assert.assertNull(userCredentials);
    }
}
