package com.rijatru.development.useraccountmanager.mockedDependencies;

import android.content.Context;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component.LibraryComponent;

import androidx.test.InstrumentationRegistry;

public class MockedApp implements UserAccountManagerApp {

    @Override
    public LibraryComponent getLibraryComponent() {
        return null;
    }

    @Override
    public Context getApplicationContext() {
        return InstrumentationRegistry.getTargetContext();
    }
}
