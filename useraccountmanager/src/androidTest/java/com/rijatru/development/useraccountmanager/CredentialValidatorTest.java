package com.rijatru.development.useraccountmanager;

import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidator;
import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidatorImplementation;

import org.junit.Assert;
import org.junit.Test;

public class CredentialValidatorTest {
    @Test
    public void isValidEmail_validEmail_returnsTrue() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertTrue(credentialsValidator.isValidEmail("email@provider.co"));
    }

    @Test
    public void isValidEmail_invalidEmail_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidEmail("email@provider"));
    }

    @Test
    public void isValidEmail_nullEmail_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidEmail(null));
    }

    @Test
    public void isPassword_validPassword_returnsTrue() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertTrue(credentialsValidator.isValidPassword("Password@98"));
    }

    @Test
    public void isPassword_shortPassword_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidPassword("Passwor"));
    }

    @Test
    public void isPassword_passwordNoUpperCaseLetters_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidPassword("password@98"));
    }

    @Test
    public void isPassword_passwordNoLowerCaseLetters_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidPassword("PASSWORD@98"));
    }

    @Test
    public void isPassword_passwordNoNumbers_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidPassword("password@"));
    }

    @Test
    public void isPassword_passwordNoSpecialCharacters_returnsFalse() {
        CredentialsValidator credentialsValidator = new CredentialsValidatorImplementation();
        Assert.assertFalse(credentialsValidator.isValidPassword("password44"));
    }
}
