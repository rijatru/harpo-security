package com.rijatru.development.useraccountmanager;

import android.content.Context;

import com.rijatru.development.useraccountmanager.data.managers.AppApiManager;
import com.rijatru.development.useraccountmanager.data.managers.RetroFitManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.services.ApiConfig;
import com.rijatru.development.useraccountmanager.data.services.timeZone.get.TimeZoneResponse;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedApiConfigImplementation;
import com.rijatru.development.useraccountmanager.mockedDependencies.MockedAppConnectivityManager;
import com.rijatru.development.useraccountmanager.rules.TestSchedulerRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import io.reactivex.observers.TestObserver;

import static java.util.concurrent.TimeUnit.SECONDS;

@RunWith(AndroidJUnit4.class)
public class ApiManagerInstrumentedTest {

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    private Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void getTimeZone_validLatLng_validResponse() {

        ApiConfig apiConfig = new MockedApiConfigImplementation();
        HttpManager httpManager = new RetroFitManager(apiConfig);
        ConnectivityStatusManager connectivityStatusManager = new MockedAppConnectivityManager();
        ApiManager apiManager = new AppApiManager(httpManager, apiConfig, connectivityStatusManager);

        TestObserver<TimeZoneResponse> testObserver = apiManager.getTimeZone(4.6357, -74.0733).test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(2, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(timeZoneResponse -> {
                    return timeZoneResponse.time != null && timeZoneResponse.hasValidTime();
                });
    }

    @Test
    public void getTimeZone_zeroLatLng_emptyResponse() {

        ApiConfig apiConfig = new MockedApiConfigImplementation();
        HttpManager httpManager = new RetroFitManager(apiConfig);
        ConnectivityStatusManager connectivityStatusManager = new MockedAppConnectivityManager();
        ApiManager apiManager = new AppApiManager(httpManager, apiConfig, connectivityStatusManager);

        TestObserver<TimeZoneResponse> testObserver = apiManager.getTimeZone(0, 0).test();

        testSchedulerRule.getTestScheduler().advanceTimeBy(2, SECONDS);

        testObserver
                .assertNoErrors()
                .assertValue(timeZoneResponse -> {
                    return timeZoneResponse.gmtOffset == 0 &&
                            timeZoneResponse.rawOffset == 0 &&
                            timeZoneResponse.dstOffset == 0;
                });
    }
}
