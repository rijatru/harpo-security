package com.rijatru.development.useraccountmanager.mockedDependencies;

import com.rijatru.development.useraccountmanager.data.services.ApiConfig;

public class MockedApiConfigImplementation implements ApiConfig {
    @Override
    public String getGeoNamesApiUrl() {
        return "http://api.geonames.org";
    }
}
