package com.rijatru.development.useraccountmanager.data.services.timeZone.get;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeZoneResponse {

    @SerializedName("sunrise")
    @Expose
    public String sunrise;
    @SerializedName("lng")
    @Expose
    public Double lng;
    @SerializedName("countryCode")
    @Expose
    public String countryCode;
    @SerializedName("gmtOffset")
    @Expose
    public Integer gmtOffset;
    @SerializedName("rawOffset")
    @Expose
    public Integer rawOffset;
    @SerializedName("sunset")
    @Expose
    public String sunset;
    @SerializedName("timezoneId")
    @Expose
    public String timezoneId;
    @SerializedName("dstOffset")
    @Expose
    public Integer dstOffset;
    @SerializedName("countryName")
    @Expose
    public String countryName;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("lat")
    @Expose
    public Double lat;

    public boolean hasValidTime() {
        return time != null && time.length() > 0;
    }
}
