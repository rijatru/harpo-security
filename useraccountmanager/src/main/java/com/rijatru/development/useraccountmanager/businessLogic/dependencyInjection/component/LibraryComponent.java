package com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.LibraryModule;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.DataModule;
import com.rijatru.development.useraccountmanager.businessLogic.encryption.Encryptor;
import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidator;
import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibraryModule.class, DataModule.class})
public interface LibraryComponent {

    UserAccountManagerApp app();

    Encryptor encryptor();

    UserCredentialsManager userCredentialsManager();

    HttpManager httpManager();

    ApiManager apiManager();

    PersistenceManager persistenceManager();

    CustomLocationManager customLocationManager();

    CredentialsValidator credentialsValidator();
}
