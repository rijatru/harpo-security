package com.rijatru.development.useraccountmanager.data.model.userCredentials;

import android.os.Parcel;
import android.os.Parcelable;

public class UserCredentials implements Parcelable {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
    }

    public UserCredentials() {
    }

    public UserCredentials(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
    }

    public static final Creator<UserCredentials> CREATOR = new Creator<UserCredentials>() {
        @Override
        public UserCredentials createFromParcel(Parcel source) {
            return new UserCredentials(source);
        }

        @Override
        public UserCredentials[] newArray(int size) {
            return new UserCredentials[size];
        }
    };

    public boolean areEqualTo(UserCredentials userCredentials) {
        return userCredentials.getUsername().equals(username) && userCredentials.getPassword().equals(password);
    }
}
