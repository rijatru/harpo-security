package com.rijatru.development.useraccountmanager.businessLogic.utils;

import android.text.TextUtils;

public class CredentialsValidatorImplementation implements CredentialsValidator {

    @Override
    public boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    @Override
    public boolean isValidPassword(String password) {
        if (password == null || password.length() < 8) {
            return false;
        }
        int count = 0;
        if (password.matches(".*\\d.*")) {
            count++;
        }
        if (password.matches(".*[a-z].*")) {
            count++;
        }
        if (password.matches(".*[A-Z].*")) {
            count++;
        }
        if (password.matches(".*?[#¿?¡!@\",.+´¨'=(){}_$%^&*/\\-].*")) {
            count++;
        }
        return count >= 4;
    }
}
