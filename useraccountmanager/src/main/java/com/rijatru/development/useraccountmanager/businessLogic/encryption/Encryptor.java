package com.rijatru.development.useraccountmanager.businessLogic.encryption;

import java.util.HashMap;

public interface Encryptor {

    HashMap<String, byte[]> encryptData(byte[] plainTextBytes, String passwordString);

    byte[] decryptData(HashMap<String, byte[]> map, String passwordString);
}
