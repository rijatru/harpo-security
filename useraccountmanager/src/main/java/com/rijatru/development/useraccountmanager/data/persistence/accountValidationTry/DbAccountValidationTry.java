package com.rijatru.development.useraccountmanager.data.persistence.accountValidationTry;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DbAccountValidationTry {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "tryDate")
    public String tryDate;

    @ColumnInfo(name = "success")
    public boolean success;
}
