package com.rijatru.development.useraccountmanager.data.managers;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class LocationManagerImplementation implements CustomLocationManager {

    private final UserAccountManagerApp app;
    private final PublishSubject<Boolean> locationPermissionRequiredSubject = PublishSubject.create();
    protected final PublishSubject<Location> locationSubject = PublishSubject.create();
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location lastKnownLocation;

    @Inject
    public LocationManagerImplementation(UserAccountManagerApp app) {
        this.app = app;
    }

    @Override
    public Observable<Boolean> getLocationPermissionRequiredObservable() {
        return locationPermissionRequiredSubject;
    }

    @Override
    public Observable<Location> getLocationObservable() {
        return locationSubject;
    }

    @Override
    public void getLocation() {
        if (lastKnownLocation != null) {
            locationSubject.onNext(lastKnownLocation);
            return;
        }
        if (locationManager == null) {
            locationManager = (LocationManager) app.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
        if (locationListener == null) {
            locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    lastKnownLocation = location;
                    clearListener();
                    locationSubject.onNext(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
        }
        if (app.getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && app.getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationPermissionRequiredSubject.onNext(true);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public boolean hasValidLocation() {
        return lastKnownLocation != null;
    }

    @Override
    public void clearListener() {
        if (locationListener != null) {
            locationManager.removeUpdates(locationListener);
            locationListener = null;
        }
    }

    @Override
    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }
}
