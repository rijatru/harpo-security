package com.rijatru.development.useraccountmanager.businessLogic.encryption;

import android.util.Log;

import java.security.SecureRandom;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptionUtil implements Encryptor {

    public static String SALT = "salt";
    public static String INITIALIZATION_VECTOR = "initializationVector";
    public static String ENCRYPTED_BYTES = "encryptedBytes";

    @Override
    public HashMap<String, byte[]> encryptData(byte[] plainTextBytes, String passwordString) {
        if (plainTextBytes == null || passwordString.length() == 0) {
            Log.e(getClass().getSimpleName(), "plainTextBytes and passwordString cannot be empty or null");
            return null;
        }
        HashMap<String, byte[]> map = new HashMap<>();

        try {
            //Random salt for next step
            SecureRandom random = new SecureRandom();
            byte salt[] = new byte[256];
            random.nextBytes(salt);

            //PBKDF2 - derive the key from the password, don't use passwords directly
            char[] passwordChar = passwordString.toCharArray(); //Turn password into char[] array
            PBEKeySpec pbKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 128); // http://www.rfc-editor.org/rfc/rfc2898.txt for iteration count documentation
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbKeySpec).getEncoded();
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "AES");

            //Create initialization vector for AES
            SecureRandom ivRandom = new SecureRandom(); //not caching previous seeded instance of SecureRandom
            byte[] iv = new byte[16];
            ivRandom.nextBytes(iv);
            IvParameterSpec ivSpec = new IvParameterSpec(iv);

            //Encrypt
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec);
            byte[] encrypted = cipher.doFinal(plainTextBytes);

            map.put(SALT, salt);
            map.put(INITIALIZATION_VECTOR, iv);
            map.put(ENCRYPTED_BYTES, encrypted);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "encryptor exception", e);
        }

        return map;
    }

    @Override
    public byte[] decryptData(HashMap<String, byte[]> map, String passwordString) {
        byte[] decrypted = null;
        try {
            byte salt[] = map.get(SALT);
            byte iv[] = map.get(INITIALIZATION_VECTOR);
            byte encrypted[] = map.get(ENCRYPTED_BYTES);

            //regenerate key from password
            char[] passwordChar = passwordString.toCharArray();
            PBEKeySpec pbKeySpec = new PBEKeySpec(passwordChar, salt, 1324, 128); // http://www.rfc-editor.org/rfc/rfc2898.txt for iteration count documentation
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] keyBytes = secretKeyFactory.generateSecret(pbKeySpec).getEncoded();
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

            //Decrypt
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            decrypted = cipher.doFinal(encrypted);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "decryptor exception", e);
        }

        return decrypted;
    }
}
