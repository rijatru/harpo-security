package com.rijatru.development.useraccountmanager.data.interfaces;

import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;

import java.util.List;

import io.reactivex.Observable;

public interface PersistenceManager {

    Observable<List<AccountValidationTry>> getAccountValidationTriesObservable();

    Observable<Boolean> getSavedValidationObservable();

    Observable<AccountValidationTry> saveAccountValidationTry(AccountValidationTry validationTry);

    Observable<List<AccountValidationTry>> getAllAccountValidationTries();

    Observable<Integer> deleteAllAccountValidationTries();

    Observable<Boolean> saveValidationTry(boolean isValidAttempt);
}
