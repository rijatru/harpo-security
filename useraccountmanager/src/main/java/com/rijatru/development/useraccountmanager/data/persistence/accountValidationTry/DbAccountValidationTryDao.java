package com.rijatru.development.useraccountmanager.data.persistence.accountValidationTry;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface DbAccountValidationTryDao {
    @Query("SELECT * FROM dbAccountValidationTry")
    List<DbAccountValidationTry> getAll();

    @Insert
    long insert(DbAccountValidationTry userCredentials);

    @Query("DELETE FROM dbAccountValidationTry")
    int deleteAll();
}
