package com.rijatru.development.useraccountmanager;

import android.app.Application;

import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component.DaggerLibraryComponent;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component.LibraryComponent;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.LibraryModule;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.DataModule;

public class UserAccountManagerAppImplementation extends Application implements UserAccountManagerApp {

    private static UserAccountManagerApp app;
    private LibraryComponent libraryComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (app == null) {
            app = this;
        }

        libraryComponent = DaggerLibraryComponent.builder()
                .libraryModule(new LibraryModule(this))
                .dataModule(new DataModule())
                .build();
    }

    @Override
    public LibraryComponent getLibraryComponent() {
        return libraryComponent;
    }

    public static UserAccountManagerApp getInstance() {
        return app;
    }
}
