package com.rijatru.development.useraccountmanager.views.interfaces;

public interface Item {

    int getType();
}
