package com.rijatru.development.useraccountmanager.data.interfaces;

import android.location.Location;

import io.reactivex.Observable;

public interface CustomLocationManager {

    Observable<Boolean> getLocationPermissionRequiredObservable();

    Observable<Location> getLocationObservable();

    void getLocation();

    boolean hasValidLocation();

    void clearListener();

    Location getLastKnownLocation();
}
