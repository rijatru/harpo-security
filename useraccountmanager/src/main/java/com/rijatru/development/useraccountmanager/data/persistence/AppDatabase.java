package com.rijatru.development.useraccountmanager.data.persistence;

import com.rijatru.development.useraccountmanager.data.persistence.accountValidationTry.DbAccountValidationTry;
import com.rijatru.development.useraccountmanager.data.persistence.accountValidationTry.DbAccountValidationTryDao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {DbAccountValidationTry.class}, version = 7, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract DbAccountValidationTryDao dbAccountValidationTryDao();
}
