package com.rijatru.development.useraccountmanager.data.services.timeZone;

import com.rijatru.development.useraccountmanager.data.services.timeZone.get.TimeZoneResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TimeZoneApi {

    @GET("timezoneJSON?formatted=true&username=qa_mobile_easy&style=full")
    Observable<TimeZoneResponse> getTimeZone(@Query("lat") double lat, @Query("lng") double lng);
}
