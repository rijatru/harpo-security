package com.rijatru.development.useraccountmanager.data.managers;

import android.util.Log;

import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.services.ApiConfig;
import com.rijatru.development.useraccountmanager.data.services.timeZone.TimeZoneApi;
import com.rijatru.development.useraccountmanager.data.services.timeZone.get.TimeZoneResponse;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class AppApiManager implements ApiManager {

    private final HttpManager httpManager;
    private final ApiConfig apiConfig;
    private final ConnectivityStatusManager connectivityStatusManager;

    private TimeZoneApi timeZoneApi;

    public AppApiManager(HttpManager httpManager, ApiConfig apiConfig, ConnectivityStatusManager connectivityStatusManager) {
        this.httpManager = httpManager;
        this.apiConfig = apiConfig;
        this.connectivityStatusManager = connectivityStatusManager;
        initApi();
    }

    @Override
    public void initApi() {
        this.timeZoneApi = httpManager.getOmdbRetrofit().create(TimeZoneApi.class);
    }

    @Override
    public Observable<TimeZoneResponse> getTimeZone(double lat, double lng) {
        return timeZoneApi.getTimeZone(lat, lng)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnError(error -> Log.e(getClass().getName() + " getTimeZone error: ", error.getMessage()))
                .onErrorReturn(error -> new TimeZoneResponse());
    }
}
