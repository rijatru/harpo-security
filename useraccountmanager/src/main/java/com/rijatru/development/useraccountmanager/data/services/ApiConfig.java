package com.rijatru.development.useraccountmanager.data.services;

public interface ApiConfig {

    String getGeoNamesApiUrl();
}
