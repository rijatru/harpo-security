package com.rijatru.development.useraccountmanager.data.managers;

import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;

public class AppConnectivityManager implements ConnectivityStatusManager {

    private boolean isConnected;

    public AppConnectivityManager() {

    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }
}
