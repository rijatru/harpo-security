package com.rijatru.development.useraccountmanager.data.interfaces;

import retrofit2.Retrofit;

public interface HttpManager {

    Retrofit getOmdbRetrofit();
}
