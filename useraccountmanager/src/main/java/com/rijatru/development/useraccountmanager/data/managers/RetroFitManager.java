package com.rijatru.development.useraccountmanager.data.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.serialization.InternalIdModelExclusionStrategy;
import com.rijatru.development.useraccountmanager.data.serialization.NullOnEmptyConverterFactory;
import com.rijatru.development.useraccountmanager.data.services.ApiConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitManager implements HttpManager {

    private final ApiConfig apiConfig;

    public RetroFitManager(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    @Override
    public Retrofit getOmdbRetrofit() {
        Gson gson = new GsonBuilder()
                .addSerializationExclusionStrategy(new InternalIdModelExclusionStrategy())
                .serializeNulls()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl(apiConfig.getGeoNamesApiUrl())
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient.build())
                .build();
    }
}
