package com.rijatru.development.useraccountmanager.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.rijatru.development.useraccountmanager.views.interfaces.Item;

public class AccountValidationTry implements Item, Parcelable {

    private long id;
    private String tryDate;
    private boolean success;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTryDate() {
        if (tryDate == null) {
            tryDate = "";
        }
        return tryDate;
    }

    public void setTryDate(String tryDate) {
        this.tryDate = tryDate;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return success ? "Successful" : "Denied";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeLong(this.id);
        dest.writeString(this.tryDate);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public AccountValidationTry() {
    }

    public AccountValidationTry(Parcel in) {
        this.id = in.readLong();
        this.tryDate = in.readString();
        this.success = in.readByte() != 0;
    }

    public static final Creator<AccountValidationTry> CREATOR = new Creator<AccountValidationTry>() {
        @Override
        public AccountValidationTry createFromParcel(Parcel source) {
            return new AccountValidationTry(source);
        }

        @Override
        public AccountValidationTry[] newArray(int size) {
            return new AccountValidationTry[size];
        }
    };

    @Override
    public int getType() {
        return 0;
    }
}
