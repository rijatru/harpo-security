package com.rijatru.development.useraccountmanager.data.interfaces;

import com.rijatru.development.useraccountmanager.data.services.timeZone.get.TimeZoneResponse;

import io.reactivex.Observable;

public interface ApiManager {

    void initApi();

    Observable<TimeZoneResponse> getTimeZone(double lat, double lng);
}
