package com.rijatru.development.useraccountmanager;

import android.content.Context;

import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component.LibraryComponent;

public interface UserAccountManagerApp {

    LibraryComponent getLibraryComponent();

    Context getApplicationContext();
}
