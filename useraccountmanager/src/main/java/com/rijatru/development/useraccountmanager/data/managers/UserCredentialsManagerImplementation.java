package com.rijatru.development.useraccountmanager.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.rijatru.development.useraccountmanager.businessLogic.encryption.EncryptionUtil;
import com.rijatru.development.useraccountmanager.businessLogic.encryption.Encryptor;
import com.rijatru.development.useraccountmanager.data.model.userCredentials.UserCredentials;
import com.rijatru.development.useraccountmanager.data.utils.ParcelableUtil;

import java.util.HashMap;

import javax.inject.Inject;

public class UserCredentialsManagerImplementation implements UserCredentialsManager {

    private static String PREFERENCES_FILE_NAME = "userPreferences";

    private Encryptor encryptor;

    @Inject
    public UserCredentialsManagerImplementation(Encryptor encryptor) {
        this.encryptor = encryptor;
    }

    @Override
    public boolean saveUserCredentials(Context context, UserCredentials userCredentials) {
        if (userCredentials == null) {
            Log.e(getClass().getSimpleName(), "userCredentials cannot be empty or null");
            return false;
        }
        byte[] dataToEncrypt = ParcelableUtil.marshall(userCredentials);
        HashMap<String, byte[]> map = encryptor.encryptData(dataToEncrypt, userCredentials.getPassword());

        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE).edit();
        String saltBase64String = Base64.encodeToString(map.get(EncryptionUtil.SALT), Base64.NO_WRAP);
        String initializationVectorBase64String = Base64.encodeToString(map.get(EncryptionUtil.INITIALIZATION_VECTOR), Base64.NO_WRAP);
        String encryptedBytesBase64String = Base64.encodeToString(map.get(EncryptionUtil.ENCRYPTED_BYTES), Base64.NO_WRAP);

        editor.putString(EncryptionUtil.SALT, saltBase64String);
        editor.putString(EncryptionUtil.INITIALIZATION_VECTOR, initializationVectorBase64String);
        editor.putString(EncryptionUtil.ENCRYPTED_BYTES, encryptedBytesBase64String);

        return editor.commit();
    }

    @Override
    public UserCredentials getUserCredentials(Context context, String password) {
        if (password == null || password.length() == 0) {
            Log.e(getClass().getSimpleName(), "password cannot be empty or null");
            return null;
        }
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        String saltBase64String = preferences.getString(EncryptionUtil.SALT, "");
        String initializationVectorBase64String = preferences.getString(EncryptionUtil.INITIALIZATION_VECTOR, "");
        String encryptedBytesBase64String = preferences.getString(EncryptionUtil.ENCRYPTED_BYTES, "");

        if (saltBase64String.length() == 0 || initializationVectorBase64String.length() == 0 || encryptedBytesBase64String.length() == 0) {
            return null;
        }
        byte[] salt = Base64.decode(saltBase64String, Base64.NO_WRAP);
        byte[] initializationVector = Base64.decode(initializationVectorBase64String, Base64.NO_WRAP);
        byte[] encryptedBytes = Base64.decode(encryptedBytesBase64String, Base64.NO_WRAP);

        HashMap<String, byte[]> map = new HashMap<>();
        map.put(EncryptionUtil.SALT, salt);
        map.put(EncryptionUtil.INITIALIZATION_VECTOR, initializationVector);
        map.put(EncryptionUtil.ENCRYPTED_BYTES, encryptedBytes);

        byte[] decryptedData = encryptor.decryptData(map, password);
        if (decryptedData == null || decryptedData.length == 0) {
            return null;
        }

        return ParcelableUtil.unMarshall(decryptedData, UserCredentials.CREATOR);
    }
}
