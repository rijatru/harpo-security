package com.rijatru.development.useraccountmanager.data.services;

import com.rijatru.development.useraccountmanager.R;
import com.rijatru.development.useraccountmanager.UserAccountManagerApp;

public class ApiConfigImplementation implements ApiConfig {

    private UserAccountManagerApp app;

    public ApiConfigImplementation(UserAccountManagerApp app) {
        this.app = app;
    }

    @Override
    public String getGeoNamesApiUrl() {
        return app.getApplicationContext().getString(R.string.geo_names_api_url);
    }
}
