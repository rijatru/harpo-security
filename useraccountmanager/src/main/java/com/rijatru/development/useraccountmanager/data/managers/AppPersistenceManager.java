package com.rijatru.development.useraccountmanager.data.managers;

import android.location.Location;
import android.util.Log;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;
import com.rijatru.development.useraccountmanager.data.persistence.AppDatabase;
import com.rijatru.development.useraccountmanager.data.persistence.accountValidationTry.DbAccountValidationTry;
import com.rijatru.development.useraccountmanager.data.services.timeZone.get.TimeZoneResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.room.Room;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class AppPersistenceManager implements PersistenceManager {

    private final ApiManager apiManager;
    private final CustomLocationManager customLocationManager;

    private final PublishSubject<List<AccountValidationTry>> validationTriesSubject = PublishSubject.create();
    private final PublishSubject<Boolean> savedValidationSubject = PublishSubject.create();

    protected CompositeDisposable disposables;

    private AppDatabase db;

    @Inject
    public AppPersistenceManager(UserAccountManagerApp app, ApiManager apiManager, CustomLocationManager customLocationManager) {
        this.apiManager = apiManager;
        this.customLocationManager = customLocationManager;
        db = Room.databaseBuilder(app.getApplicationContext(),
                AppDatabase.class, "database-easy-solutions")
                .fallbackToDestructiveMigration()
                .build();
        disposables = new CompositeDisposable();
    }

    @Override
    public Observable<List<AccountValidationTry>> getAccountValidationTriesObservable() {
        return validationTriesSubject;
    }

    @Override
    public Observable<Boolean> getSavedValidationObservable() {
        return savedValidationSubject;
    }

    @Override
    public Observable<AccountValidationTry> saveAccountValidationTry(AccountValidationTry validationTry) {
        return Observable.create(subscriber -> {
            if (validationTry == null || validationTry.getTryDate().length() == 0) {
                subscriber.onError(new Throwable("Invalid validation entry object"));
                subscriber.onComplete();
            } else {
                DbAccountValidationTry dbAccountValidationTry = new DbAccountValidationTry();
                dbAccountValidationTry.tryDate = validationTry.getTryDate();
                dbAccountValidationTry.success = validationTry.isSuccess();

                long id = db.dbAccountValidationTryDao().insert(dbAccountValidationTry);
                validationTry.setId(id);
                subscriber.onNext(validationTry);
                subscriber.onComplete();
            }
        });
    }

    @Override
    public Observable<List<AccountValidationTry>> getAllAccountValidationTries() {
        return Observable.create(subscriber -> {
            List<DbAccountValidationTry> dbAccountValidationTryList = db.dbAccountValidationTryDao().getAll();
            List<AccountValidationTry> accountValidationTries = new ArrayList<>();

            for (DbAccountValidationTry dbTry : dbAccountValidationTryList) {
                AccountValidationTry accountValidationTry = new AccountValidationTry();
                accountValidationTry.setId(dbTry.id);
                accountValidationTry.setTryDate(dbTry.tryDate);
                accountValidationTry.setSuccess(dbTry.success);

                accountValidationTries.add(accountValidationTry);
            }

            validationTriesSubject.onNext(accountValidationTries);

            subscriber.onNext(accountValidationTries);
            subscriber.onComplete();
        });
    }

    @Override
    public Observable<Integer> deleteAllAccountValidationTries() {
        return Observable.create(subscriber -> {
            int deletedRows = db.dbAccountValidationTryDao().deleteAll();
            subscriber.onNext(deletedRows);
            subscriber.onComplete();
        });
    }

    @Override
    public Observable<Boolean> saveValidationTry(boolean isValidAttempt) {
        return Observable.create(subscriber -> {
            Location location = customLocationManager.getLastKnownLocation();
            if (location != null) {
                disposables.clear();
                disposables.add(apiManager.getTimeZone(location.getLatitude(), location.getLongitude())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .doOnError(error -> Log.e(getClass().getSimpleName(), error.getMessage()))
                        .onErrorReturn(error -> new TimeZoneResponse())
                        .subscribe(timeZoneResponse -> {
                            if (timeZoneResponse.hasValidTime()) {
                                AccountValidationTry accountValidationTry = new AccountValidationTry();
                                accountValidationTry.setTryDate(timeZoneResponse.time);
                                accountValidationTry.setSuccess(isValidAttempt);

                                saveAccountValidationTry(accountValidationTry)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(Schedulers.io())
                                        .doOnError(error -> Log.e(getClass().getSimpleName(), error.getMessage()))
                                        .onErrorReturn(error -> new AccountValidationTry())
                                        .subscribe(result -> {
                                            if (result.getId() > 0) {
                                                subscriber.onNext(true);
                                            } else {
                                                subscriber.onNext(false);
                                            }
                                            savedValidationSubject.onNext(isValidAttempt);
                                            subscriber.onComplete();
                                        });
                            }
                        })
                );
            }
        });
    }
}
