package com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LibraryModule {

    protected final UserAccountManagerApp app;

    public LibraryModule(UserAccountManagerApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public UserAccountManagerApp app() {
        return app;
    }
}
