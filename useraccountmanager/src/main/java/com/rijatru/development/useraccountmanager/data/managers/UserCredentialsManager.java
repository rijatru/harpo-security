package com.rijatru.development.useraccountmanager.data.managers;

import android.content.Context;

import com.rijatru.development.useraccountmanager.data.model.userCredentials.UserCredentials;

public interface UserCredentialsManager {

    boolean saveUserCredentials(Context context, UserCredentials userCredentials);

    UserCredentials getUserCredentials(Context context, String password);
}
