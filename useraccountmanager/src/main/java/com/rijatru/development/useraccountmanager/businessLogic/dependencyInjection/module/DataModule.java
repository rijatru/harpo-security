package com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.businessLogic.encryption.EncryptionUtil;
import com.rijatru.development.useraccountmanager.businessLogic.encryption.Encryptor;
import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidator;
import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidatorImplementation;
import com.rijatru.development.useraccountmanager.data.interfaces.ApiManager;
import com.rijatru.development.useraccountmanager.data.interfaces.ConnectivityStatusManager;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.interfaces.HttpManager;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.useraccountmanager.data.managers.AppApiManager;
import com.rijatru.development.useraccountmanager.data.managers.AppConnectivityManager;
import com.rijatru.development.useraccountmanager.data.managers.AppPersistenceManager;
import com.rijatru.development.useraccountmanager.data.managers.LocationManagerImplementation;
import com.rijatru.development.useraccountmanager.data.managers.RetroFitManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManagerImplementation;
import com.rijatru.development.useraccountmanager.data.services.ApiConfig;
import com.rijatru.development.useraccountmanager.data.services.ApiConfigImplementation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Singleton
    @Provides
    public Encryptor encryptor() {
        return new EncryptionUtil();
    }

    @Singleton
    @Provides
    public UserCredentialsManager userCredentialsManager(Encryptor encryptor) {
        return new UserCredentialsManagerImplementation(encryptor);
    }

    @Singleton
    @Provides
    public ApiConfig apiConfig(UserAccountManagerApp app) {
        return new ApiConfigImplementation(app);
    }

    @Singleton
    @Provides
    public HttpManager httpManager(ApiConfig apiConfig) {
        return new RetroFitManager(apiConfig);
    }

    @Singleton
    @Provides
    public ApiManager apiManager(HttpManager httpManager, ApiConfig apiConfig, ConnectivityStatusManager connectivityStatusManager) {
        return new AppApiManager(httpManager, apiConfig, connectivityStatusManager);
    }

    @Singleton
    @Provides
    public ConnectivityStatusManager connectivityManager() {
        return new AppConnectivityManager();
    }

    @Singleton
    @Provides
    public PersistenceManager persistenceManager(UserAccountManagerApp app, ApiManager apiManager, CustomLocationManager customLocationManager) {
        return new AppPersistenceManager(app, apiManager, customLocationManager);
    }

    @Singleton
    @Provides
    public CustomLocationManager customLocationManager(UserAccountManagerApp app) {
        return new LocationManagerImplementation(app);
    }

    @Singleton
    @Provides
    public CredentialsValidator credentialsValidator() {
        return new CredentialsValidatorImplementation();
    }
}
