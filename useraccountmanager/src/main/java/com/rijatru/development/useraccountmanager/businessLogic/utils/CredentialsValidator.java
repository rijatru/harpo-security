package com.rijatru.development.useraccountmanager.businessLogic.utils;

public interface CredentialsValidator {

    boolean isValidEmail(String email);

    boolean isValidPassword(String password);
}
