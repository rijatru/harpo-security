package com.rijatru.development.useraccountmanager.data.interfaces;

public interface ConnectivityStatusManager {

    boolean isConnected();

    void setIsConnected(boolean isConnected);
}
