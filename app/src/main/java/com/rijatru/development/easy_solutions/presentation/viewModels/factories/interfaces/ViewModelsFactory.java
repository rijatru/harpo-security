package com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces;

import com.rijatru.development.easy_solutions.presentation.viewModels.BaseViewModel;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.AppView;

public interface ViewModelsFactory {

    BaseViewModel getViewModel(AppView view, int type);
}
