package com.rijatru.development.easy_solutions.presentation.viewModels.factories;

import com.rijatru.development.easy_solutions.presentation.viewModels.BaseViewModel;
import com.rijatru.development.easy_solutions.presentation.viewModels.ListActivityViewModel;
import com.rijatru.development.easy_solutions.presentation.viewModels.MainActivityViewModel;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.ListViewMvvm;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.MainViewMvvm;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.AppView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class AppViewModelsFactory implements ViewModelsFactory {

    public static final int MAIN_VIEW_VIEW_MODEL = 100;
    public static final int LIST_VIEW_VIEW_MODEL = 110;

    @Override
    public BaseViewModel getViewModel(AppView view, int type) {
        BaseViewModel viewModel;
        switch (type) {
            case MAIN_VIEW_VIEW_MODEL:
                viewModel = ViewModelProviders.of((AppCompatActivity) view).get(MainActivityViewModel.class);
                ((MainViewMvvm.ViewModel) viewModel).setView((MainViewMvvm.View) view);
                break;
            case LIST_VIEW_VIEW_MODEL:
                viewModel = ViewModelProviders.of((AppCompatActivity) view).get(ListActivityViewModel.class);
                ((ListViewMvvm.ViewModel) viewModel).setView((ListViewMvvm.View) view);
                break;
            default:
                viewModel = null;
        }
        return viewModel;
    }
}
