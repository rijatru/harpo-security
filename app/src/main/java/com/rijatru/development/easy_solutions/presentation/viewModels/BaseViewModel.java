package com.rijatru.development.easy_solutions.presentation.viewModels;

import com.rijatru.development.easy_solutions.AppImplementation;
import com.rijatru.development.easy_solutions.dependencyInjection.component.AppViewModelsComponent;
import com.rijatru.development.easy_solutions.dependencyInjection.component.DaggerAppViewModelsComponent;

import androidx.lifecycle.ViewModel;

public abstract class BaseViewModel extends ViewModel {

    protected AppViewModelsComponent component;

    public BaseViewModel() {
        component = DaggerAppViewModelsComponent.builder()
                .appComponent(AppImplementation.getApp().getAppComponent())
                .build();
    }

    protected AppViewModelsComponent getComponent() {
        return component;
    }
}
