package com.rijatru.development.easy_solutions.presentation.viewModels;

import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.TryItemViewMvvm;

public class TryItemViewModel extends BaseViewModel implements TryItemViewMvvm.ViewModel {

    private final AccountValidationTry accountValidationTry;

    public TryItemViewModel(AccountValidationTry accountValidationTry) {
        this.accountValidationTry = accountValidationTry;
    }

    @Override
    public AccountValidationTry getAccountValidationTry() {
        return accountValidationTry;
    }
}
