package com.rijatru.development.easy_solutions.presentation.viewModels.interfaces;

import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.AppView;

public interface TryItemViewMvvm {

    interface View extends AppView {

    }

    interface ViewModel {

        AccountValidationTry getAccountValidationTry();
    }
}
