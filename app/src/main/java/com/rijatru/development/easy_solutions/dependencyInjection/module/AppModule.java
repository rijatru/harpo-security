package com.rijatru.development.easy_solutions.dependencyInjection.module;

import com.rijatru.development.useraccountmanager.UserAccountManagerApp;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.LibraryModule;

import dagger.Module;

@Module
public class AppModule extends LibraryModule {

    public AppModule(UserAccountManagerApp app) {
        super(app);
    }
}
