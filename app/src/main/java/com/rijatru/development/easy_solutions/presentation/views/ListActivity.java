package com.rijatru.development.easy_solutions.presentation.views;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.rijatru.development.easy_solutions.R;
import com.rijatru.development.easy_solutions.databinding.ActivityListBinding;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.AppViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.ListViewMvvm;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListAdapter;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.services.notifications.LibraryNotificationListenerService;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListActivity extends BaseActivity implements ListViewMvvm.View {

    private ActivityListBinding binding;
    private ListViewMvvm.ViewModel viewModel;
    private NotificationReceiver notificationReceiver;

    @Inject
    ViewModelsFactory viewModelsFactory;

    @Inject
    CustomLocationManager customLocationManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);

        viewModel = (ListViewMvvm.ViewModel) viewModelsFactory.getViewModel(this, AppViewModelsFactory.LIST_VIEW_VIEW_MODEL);
        binding.setViewModel(viewModel);

        notificationReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.rijatru.development.useraccountmanager.NOTIFICATION_LISTENER_EXAMPLE");
        registerReceiver(notificationReceiver, filter);

        createNotification();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (viewModel != null) {
            viewModel.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel != null) {
            viewModel.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    public void onListAdapterReady(ListAdapter adapter) {
        binding.list.setLayoutManager(new LinearLayoutManager(this));
        binding.list.setAdapter((RecyclerView.Adapter) adapter);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onLogOut() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void createNotification() {
        startService(new Intent(this, LibraryNotificationListenerService.class));

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String id = "easy-solutions";
        CharSequence name = "name";
        String description = "description";

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, id);
        NotificationChannel channel;

        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_LOW;
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(id, name, importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }

        notificationBuilder.setContentTitle("Welcome!");
        notificationBuilder.setContentText("easy-solutions");
        notificationBuilder.setTicker("Test notification");
        notificationBuilder.setSmallIcon(R.drawable.ic_launcher_foreground);
        notificationBuilder.setAutoCancel(true);
        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }

    private class NotificationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra("notification_event");
            Log.d(getClass().getSimpleName(), "onreceive: " + message);
        }
    }
}
