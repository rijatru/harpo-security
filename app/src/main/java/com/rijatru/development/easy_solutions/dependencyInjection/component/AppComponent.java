package com.rijatru.development.easy_solutions.dependencyInjection.component;

import com.rijatru.development.easy_solutions.dependencyInjection.module.AppDataModule;
import com.rijatru.development.easy_solutions.dependencyInjection.module.AppModule;
import com.rijatru.development.easy_solutions.dependencyInjection.module.AppViewsModule;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ListAdapterFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ViewModelsFactory;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.component.LibraryComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, AppViewsModule.class, AppDataModule.class})
public interface AppComponent extends LibraryComponent {

    ViewModelsFactory viewModelsFactory();

    ListAdapterFactory listAdapterFactory();
}
