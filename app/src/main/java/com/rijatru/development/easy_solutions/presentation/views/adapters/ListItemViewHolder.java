package com.rijatru.development.easy_solutions.presentation.views.adapters;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    public ListItemViewHolder(View itemView) {
        super(itemView);
    }
}
