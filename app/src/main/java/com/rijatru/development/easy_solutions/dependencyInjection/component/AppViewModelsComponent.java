package com.rijatru.development.easy_solutions.dependencyInjection.component;

import com.rijatru.development.easy_solutions.presentation.viewModels.ListActivityViewModel;
import com.rijatru.development.easy_solutions.presentation.viewModels.MainActivityViewModel;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.scope.AppScope;

import dagger.Component;

@AppScope
@Component(dependencies = AppComponent.class)
public interface AppViewModelsComponent extends AppComponent {

    void inject(MainActivityViewModel mainActivityViewModel);

    void inject(ListActivityViewModel listActivityViewModel);
}
