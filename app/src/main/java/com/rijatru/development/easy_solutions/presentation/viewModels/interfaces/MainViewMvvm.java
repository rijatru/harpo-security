package com.rijatru.development.easy_solutions.presentation.viewModels.interfaces;

import com.rijatru.development.easy_solutions.presentation.views.interfaces.AppView;
import com.rijatru.development.useraccountmanager.data.model.userCredentials.UserCredentials;

import androidx.lifecycle.LiveData;

public interface MainViewMvvm {

    interface View extends AppView {

        void onInvalidCredentials();

        void onValidCredentials();

        void onRequiresValidLocation();

        void onInvalidEmail();

        void onInvalidPassword();
    }

    interface ViewModel {

        void setView(View view);

        void onPause();

        void onResume();

        LiveData<UserCredentials> getUserCredentials();

        LiveData<UserCredentials> getUserCredentialsSignUp();

        void onLogIn(android.view.View view);

        void onSignUp(android.view.View view);
    }
}
