package com.rijatru.development.easy_solutions.presentation.views.interfaces;

import com.rijatru.development.useraccountmanager.views.interfaces.Item;

public interface ItemView {

    void bind(Item item, int position);
}
