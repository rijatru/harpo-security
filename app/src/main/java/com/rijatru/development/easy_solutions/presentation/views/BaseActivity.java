package com.rijatru.development.easy_solutions.presentation.views;

import android.os.Bundle;

import com.rijatru.development.easy_solutions.AppImplementation;
import com.rijatru.development.easy_solutions.dependencyInjection.component.AppViewsComponent;
import com.rijatru.development.easy_solutions.dependencyInjection.component.DaggerAppViewsComponent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    protected AppViewsComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component = DaggerAppViewsComponent.builder()
                .appComponent(AppImplementation.getApp().getAppComponent())
                .build();
    }

    protected AppViewsComponent getComponent() {
        return component;
    }
}
