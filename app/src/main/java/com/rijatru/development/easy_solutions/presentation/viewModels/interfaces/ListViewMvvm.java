package com.rijatru.development.easy_solutions.presentation.viewModels.interfaces;

import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListView;

public interface ListViewMvvm {

    interface View extends ListView {

    }

    interface ViewModel {

        void setView(ListView view);

        void onPause();

        void onResume();

        void onLogOut(android.view.View view);
    }
}
