package com.rijatru.development.easy_solutions.presentation.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.rijatru.development.easy_solutions.R;
import com.rijatru.development.useraccountmanager.data.model.AccountValidationTry;
import com.rijatru.development.easy_solutions.databinding.TryItemBinding;
import com.rijatru.development.easy_solutions.presentation.viewModels.TryItemViewModel;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.TryItemViewMvvm;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ItemView;
import com.rijatru.development.useraccountmanager.views.interfaces.Item;

import androidx.databinding.DataBindingUtil;

public class TryItemView extends FrameLayout implements ItemView, TryItemViewMvvm.View {

    private TryItemBinding binding;
    private TryItemViewMvvm.ViewModel viewModel;

    public TryItemView(Context context) {
        super(context);
        init(context);
    }

    public TryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TryItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    protected void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = DataBindingUtil.inflate(inflater, R.layout.try_item, this, true);
    }

    @Override
    public void bind(Item item, int position) {
        viewModel = new TryItemViewModel((AccountValidationTry) item);
        binding.setViewModel(viewModel);
    }
}
