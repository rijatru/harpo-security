package com.rijatru.development.easy_solutions.presentation.views.factories;

import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ListAdapterFactory;
import com.rijatru.development.easy_solutions.presentation.views.adapters.AppListAdapter;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListAdapter;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListView;

public class AppListAdapterFactory implements ListAdapterFactory {

    @Override
    public ListAdapter getListAdapter(ListView view) {
        return new AppListAdapter(view);
    }
}
