package com.rijatru.development.easy_solutions.presentation.views.interfaces;

import com.rijatru.development.useraccountmanager.views.interfaces.Item;

import java.util.List;

public interface ListAdapter {

    void setItems(List<Item> listItems);

    void addNewItems(List<Item> items);

    void removeAllItems();

    void removeLastItems(int numberOfItemsToRemove);

    void addNewItemsToBeginning(List<Item> newPhotos);

    int getItemCount();

    boolean hasNoItems();

    void addItemToIndex(int index, Item item);
}
