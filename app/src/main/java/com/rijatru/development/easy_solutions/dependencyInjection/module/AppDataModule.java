package com.rijatru.development.easy_solutions.dependencyInjection.module;

import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.module.DataModule;

import dagger.Module;

@Module
public class AppDataModule extends DataModule {

}
