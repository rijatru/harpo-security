package com.rijatru.development.easy_solutions.presentation.views.interfaces;

public interface ListView extends AppView {

    void onListAdapterReady(ListAdapter adapter);

    void onLogOut();
}
