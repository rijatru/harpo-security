package com.rijatru.development.easy_solutions;

import com.rijatru.development.easy_solutions.dependencyInjection.component.AppComponent;
import com.rijatru.development.easy_solutions.dependencyInjection.component.DaggerAppComponent;
import com.rijatru.development.easy_solutions.dependencyInjection.module.AppDataModule;
import com.rijatru.development.easy_solutions.dependencyInjection.module.AppModule;
import com.rijatru.development.easy_solutions.dependencyInjection.module.AppViewsModule;
import com.rijatru.development.useraccountmanager.UserAccountManagerAppImplementation;

public class AppImplementation extends UserAccountManagerAppImplementation implements App {

    private static App app;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (app == null) {
            app = this;
        }

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .appDataModule(new AppDataModule())
                .appViewsModule(new AppViewsModule())
                .build();
    }

    public static App getApp() {
        return app;
    }

    @Override
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
