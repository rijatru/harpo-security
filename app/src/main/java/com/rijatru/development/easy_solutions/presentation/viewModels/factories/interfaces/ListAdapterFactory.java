package com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces;

import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListAdapter;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListView;

public interface ListAdapterFactory {

    ListAdapter getListAdapter(ListView view);
}
