package com.rijatru.development.easy_solutions.presentation.views.interfaces;

import android.content.Context;

public interface AppView {

    Context getContext();
}
