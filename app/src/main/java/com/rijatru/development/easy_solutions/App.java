package com.rijatru.development.easy_solutions;

import com.rijatru.development.easy_solutions.dependencyInjection.component.AppComponent;
import com.rijatru.development.useraccountmanager.UserAccountManagerApp;

public interface App extends UserAccountManagerApp {

    AppComponent getAppComponent();
}
