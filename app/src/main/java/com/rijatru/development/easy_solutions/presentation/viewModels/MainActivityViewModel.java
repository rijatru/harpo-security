package com.rijatru.development.easy_solutions.presentation.viewModels;

import android.view.View;

import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.MainViewMvvm;
import com.rijatru.development.useraccountmanager.businessLogic.utils.CredentialsValidator;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;
import com.rijatru.development.useraccountmanager.data.model.userCredentials.UserCredentials;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends BaseViewModel implements MainViewMvvm.ViewModel {

    private MainViewMvvm.View view;
    private MutableLiveData<UserCredentials> userCredentials;
    private MutableLiveData<UserCredentials> userCredentialsSignUp;
    private boolean isOnAction;

    protected CompositeDisposable disposables;

    @Inject
    UserCredentialsManager userCredentialsManager;

    @Inject
    PersistenceManager persistenceManager;

    @Inject
    CustomLocationManager customLocationManager;

    @Inject
    CredentialsValidator credentialsValidator;

    public MainActivityViewModel() {
        getComponent().inject(this);
        disposables = new CompositeDisposable();
    }

    @Override
    public void setView(MainViewMvvm.View view) {
        this.view = view;
    }

    @Override
    public void onPause() {
        disposables.clear();
    }

    @Override
    public void onResume() {
        subscribeToObservables();
    }

    private void subscribeToObservables() {
        disposables.clear();
        disposables.add(persistenceManager.getSavedValidationObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(isValidAttempt -> {
                    isOnAction = false;
                    if (isValidAttempt) {
                        this.view.onValidCredentials();
                    } else {
                        this.view.onInvalidCredentials();
                    }
                })
                .subscribe());
    }

    @Override
    public LiveData<UserCredentials> getUserCredentials() {
        if (userCredentials == null) {
            userCredentials = new MutableLiveData<>();
            userCredentials.setValue(new UserCredentials());
        }
        return userCredentials;
    }

    @Override
    public LiveData<UserCredentials> getUserCredentialsSignUp() {
        if (userCredentialsSignUp == null) {
            userCredentialsSignUp = new MutableLiveData<>();
            userCredentialsSignUp.setValue(new UserCredentials());
        }
        return userCredentialsSignUp;
    }

    @Override
    public void onLogIn(View view) {
        String password = userCredentials.getValue().getPassword();
        if (!isOnAction) {
            isOnAction = true;
            UserCredentials credentials = userCredentialsManager.getUserCredentials(this.view.getContext(), password);
            if (credentials != null && credentials.areEqualTo(userCredentials.getValue())) {
                persistenceManager.saveValidationTry(true).subscribe();
            } else {
                persistenceManager.saveValidationTry(false).subscribe();
            }
        }
    }

    @Override
    public void onSignUp(View view) {
        String username = userCredentialsSignUp.getValue().getUsername();
        String password = userCredentialsSignUp.getValue().getPassword();
        if (!credentialsValidator.isValidEmail(username)) {
            this.view.onInvalidEmail();
        } else if (!credentialsValidator.isValidPassword(password)) {
            this.view.onInvalidPassword();
        } else {
            if (!isOnAction) {
                isOnAction = true;
                if (customLocationManager.hasValidLocation()) {
                    boolean credentialsSaved = userCredentialsManager.saveUserCredentials(this.view.getContext(), userCredentialsSignUp.getValue());
                    if (credentialsSaved) {
                        persistenceManager.deleteAllAccountValidationTries()
                                .subscribeOn(Schedulers.io())
                                .subscribe();
                        isOnAction = false;
                        this.view.onValidCredentials();
                    }
                } else {
                    this.view.onRequiresValidLocation();
                }
            }
        }
    }
}
