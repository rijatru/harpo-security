package com.rijatru.development.easy_solutions.dependencyInjection.module;

import com.rijatru.development.easy_solutions.presentation.viewModels.factories.AppViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ListAdapterFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.views.factories.AppListAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppViewsModule {

    @Singleton
    @Provides
    public ViewModelsFactory viewModelsFactory() {
        return new AppViewModelsFactory();
    }

    @Singleton
    @Provides
    public ListAdapterFactory listAdapterFactory() {
        return new AppListAdapterFactory();
    }
}
