package com.rijatru.development.easy_solutions.presentation.viewModels;

import android.view.View;

import com.google.common.collect.Lists;
import com.rijatru.development.useraccountmanager.data.interfaces.PersistenceManager;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ListAdapterFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.ListViewMvvm;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListAdapter;
import com.rijatru.development.easy_solutions.presentation.views.interfaces.ListView;
import com.rijatru.development.useraccountmanager.data.managers.UserCredentialsManager;
import com.rijatru.development.useraccountmanager.views.interfaces.Item;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ListActivityViewModel extends BaseViewModel implements ListViewMvvm.ViewModel {

    private ListView view;
    private ListAdapter adapter;

    protected CompositeDisposable disposables;

    @Inject
    PersistenceManager persistenceManager;

    @Inject
    ListAdapterFactory listAdapterFactory;

    @Inject
    UserCredentialsManager userCredentialsManager;

    public ListActivityViewModel() {
        getComponent().inject(this);
        disposables = new CompositeDisposable();
    }

    @Override
    public void setView(ListView view) {
        this.view = view;
        initAdapter();
    }

    @Override
    public void onPause() {
        disposables.clear();
    }

    @Override
    public void onResume() {
        subscribeToObservables();
    }

    private void initAdapter() {
        if (adapter == null) {
            adapter = listAdapterFactory.getListAdapter(view);
        }
        subscribeToObservables();
        persistenceManager.getAllAccountValidationTries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    private void subscribeToObservables() {
        disposables.clear();
        disposables.add(persistenceManager.getAccountValidationTriesObservable()
                .doOnNext(accountValidationTries -> {
                    List<Item> items = new ArrayList<>(accountValidationTries);
                    adapter.setItems(Lists.reverse(items));
                    view.onListAdapterReady(adapter);
                })
                .subscribe());
    }

    @Override
    public void onLogOut(View view) {
        this.view.onLogOut();
    }
}
