package com.rijatru.development.easy_solutions.dependencyInjection.component;

import com.rijatru.development.easy_solutions.presentation.views.ListActivity;
import com.rijatru.development.easy_solutions.presentation.views.MainActivity;
import com.rijatru.development.useraccountmanager.businessLogic.dependencyInjection.scope.AppScope;

import dagger.Component;

@AppScope
@Component(dependencies = AppComponent.class)
public interface AppViewsComponent extends AppComponent {

    void inject(MainActivity mainActivity);

    void inject(ListActivity mainActivity);
}
