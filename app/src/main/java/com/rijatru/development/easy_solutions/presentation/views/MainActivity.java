package com.rijatru.development.easy_solutions.presentation.views;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.rijatru.development.easy_solutions.R;
import com.rijatru.development.useraccountmanager.data.interfaces.CustomLocationManager;
import com.rijatru.development.easy_solutions.databinding.ActivityMainBinding;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.AppViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.factories.interfaces.ViewModelsFactory;
import com.rijatru.development.easy_solutions.presentation.viewModels.interfaces.MainViewMvvm;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

public class MainActivity extends BaseActivity implements MainViewMvvm.View {

    private static final int ACCESS_LOCATION_INTENT_ID = 3;

    private ActivityMainBinding binding;
    private MainViewMvvm.ViewModel viewModel;

    @Inject
    ViewModelsFactory viewModelsFactory;

    @Inject
    CustomLocationManager customLocationManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = (MainViewMvvm.ViewModel) viewModelsFactory.getViewModel(this, AppViewModelsFactory.MAIN_VIEW_VIEW_MODEL);
        viewModel.getUserCredentials().observe(this, userCredentials -> binding.setViewModel(viewModel));

        requestLocationPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (viewModel != null) {
            viewModel.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel != null) {
            viewModel.onResume();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onInvalidCredentials() {
        CharSequence text = getString(R.string.invalid_credentials);
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    @Override
    public void onValidCredentials() {
        CharSequence text = getString(R.string.Welcome);
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();

        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequiresValidLocation() {
        CharSequence text = getString(R.string.invalid_location);
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    @Override
    public void onInvalidEmail() {
        CharSequence text = getString(R.string.invalid_email);
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    @Override
    public void onInvalidPassword() {
        CharSequence text = getString(R.string.invalid_password);
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();
    }

    protected void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.e(getClass().getSimpleName(), "Show an explanation to the user *asynchronously* -- don't block");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_LOCATION_INTENT_ID);

                CharSequence text = getString(R.string.location_is_required);
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(this, text, duration);
                toast.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_LOCATION_INTENT_ID);
            }
        } else {
            requestTurnOnLocation();
        }
    }

    private void requestTurnOnLocation() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.
                Log.i(getClass().getSimpleName(), "All location settings are satisfied.");
                customLocationManager.getLocation();
            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(getClass().getSimpleName(), "All location settings are satisfied.");
                        customLocationManager.getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(getClass().getSimpleName(), "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            resolvable.startResolutionForResult(this, ACCESS_LOCATION_INTENT_ID);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(getClass().getSimpleName(), "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(getClass().getSimpleName(), "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_LOCATION_INTENT_ID: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestTurnOnLocation();
                } else {
                    Log.e(getClass().getSimpleName(), "onRequestLocationPermissionsResult -> Permission not granted!");
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ACCESS_LOCATION_INTENT_ID:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(getClass().getSimpleName(), "Auth agreed to make required location settings changes.");
                        customLocationManager.getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(getClass().getSimpleName(), "Auth chose not to make required location settings changes.");
                        requestLocationPermission();
                        break;
                }
                break;
        }
    }
}
